let button=document.getElementById('btn');

button.addEventListener('click', () =>{
    const height= parseInt(document.getElementById('height').value);
    const weight= parseInt(document.getElementById('weight').value);
    const res= document.getElementById('output');
    let height_status=false,weight_status=false;

    if(height==='' || isNaN(height) ||(height<=0)){
        document.getElementById('height-error').innerHTML='Please enter height';
    }else{
        document.getElementById('height-error').innerHTML='';
        height_status=true;
    }
    if(weight==='' || isNaN(weight) ||(weight<=0)){
        document.getElementById('weight-error').innerHTML='Please enter weight';
    }else{
        document.getElementById('weight-error').innerHTML='';
        weight_status=true;
    }

    if (height_status&&weight_status) {
        const bmi=(weight/((height*height)/10000)).toFixed(2);
        // res.innerHTML=bmi;
        if (bmi<=18.6) {
            res.innerHTML='under weight :'+ bmi;
        } else if(bmi=>18.6 && bmi < 24.9) {
            res.innerHTML='Normal :'+ bmi;
        }else{
            res.innerHTML='over weight ;'+ bmi; 
        }
    }else{
        alert('This has error')
        res.innerHTML='';
    }
})